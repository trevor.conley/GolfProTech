module.exports = {
  apps: [{
    name: 'GolfProTech',
    script: './index.js'
  }],
  deploy: {
    production: {
      user: 'ubuntu',
      host: 'ec2-34-227-74-39.compute-1.amazonaws.com',
      key: '~/.ssh/ec2.pem',
      ref: 'origin/master',
      repo: 'git@gitlab.com:trevor.conley/GolfProTech.git',
      path: '/home/ubuntu/GolfProTech',
      'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js'
    }
  }
}
